import telepot
import yaml
import datetime
import logging

from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton

from miners.claymore_miner import ClaymoreMiner

configs = None
global update_id
update_id = 0
logger = logging.getLogger(__name__)

with open("config.yml", 'r') as stream:
    configs = yaml.load(stream)

TelegramBot = telepot.Bot(configs['telegram']['token'])
markup = ReplyKeyboardMarkup(keyboard=[[KeyboardButton(text='Status')]], one_time_keyboard=False)


def handle(msg):
    chat_id = msg['chat']['id']
    if chat_id == configs['telegram']['chat_id']:
        commandfull = msg['text'].strip().lower()
        command = commandfull.split('@')[0]

        if command == 'status':
            send_full_status()


def send_full_status():
    for r in configs['rigs']:
        miner = ClaymoreMiner(rig_name=r['rig_name'], host=r['host'], port=r['port'],
                              installed_gpu=r['installed_gpu'])
        status_rig = miner.get_status_rig()
        status_gpu = miner.get_status_gpu()
        if r['miner'] == "claymore_cryptonight":
            text_sender = "Rig: *" + miner.rig_name + '*\n'
            text_sender += "Pool: *" + status_rig.pool + '*\n'
            text_sender += "Miner: *" + status_rig.miner_version + '*\n'
            text_sender += "Uptime:* " + str(datetime.timedelta(seconds=(int(status_rig.mins_up) * 60))) + '*\n'
            text_sender += 'Hashrate Total:* ' + str(
                "{:,.2f}".format(status_rig.hashrate_mhs)) + '* H/s' + '\n'
            for gg in status_gpu:
                text_gpu = "GPU:* " + str(gg.rig_gpu_id) + "*"
                text_gpu += " H/s:*" + str("{:,.2f}".format(int(gg.gpu_hashrate))) + "*"
                text_gpu += " Temp:* " + str(gg.gpu_temp) + "*C"
                text_gpu += " Fan:* " + str(gg.gpu_fan) + "%*" + '\n'
                text_sender += text_gpu

        else:
            text_sender = "Rig: *" + miner.rig_name + '*\n'
            text_sender += "Pool: *" + status_rig.pool + '*\n'
            text_sender += "Miner: *" + status_rig.miner_version + '*\n'
            text_sender += "Uptime:* " + str(datetime.timedelta(seconds=(int(status_rig.mins_up) * 60))) + '*\n'
            text_sender += 'Hashrate Total:* ' + str(
                "{:,.2f}".format(status_rig.hashrate_mhs / float(1000))) + '* Mh/s' + '\n'
            for gg in status_gpu:
                text_gpu = "GPU:* " + str(gg.rig_gpu_id) + "*"
                text_gpu += " Mh/s:*" + str("{:,.2f}".format(int(gg.gpu_hashrate) / float(1000))) + "*"
                text_gpu += " Temp:* " + str(gg.gpu_temp) + "*C"
                text_gpu += " Fan:* " + str(gg.gpu_fan) + "%*" + '\n'
                text_sender += text_gpu

        send_message(text_sender)


def send_status():
    for r in configs['rigs']:
        miner = ClaymoreMiner(rig_name=r['rig_name'], host=r['host'], port=r['port'],
                              installed_gpu=r['installed_gpu'])
        status_rig = miner.get_status_rig()
        status_gpu = miner.get_status_gpu()

        if r['miner'] == "claymore_cryptonight":
            count_gpu = 0
            for gg in status_gpu:
                if int(gg.gpu_hashrate) > 0:
                    count_gpu += 1
            text_sender = " Status: Rig:* " + miner.rig_name + "* "
            text_sender += "GPUS:* " + str(count_gpu) + '*  '
            text_sender += 'Hashrate Total:* ' + str(
                "{:,.2f}".format(status_rig.hashrate_mhs )) + ' H/s*' + '\n'
        else:
            count_gpu = 0
            for gg in status_gpu:
                if int(gg.gpu_hashrate) > 0:
                    count_gpu += 1
            text_sender = " Status: Rig:* " + miner.rig_name + "* "
            text_sender += "GPUS:* " + str(count_gpu) + '*  '
            text_sender += 'Hashrate Total:* ' + str(
                "{:,.2f}".format(status_rig.hashrate_mhs / float(1000))) + ' Mh/s*' + '\n'

    send_message(text_sender)


def get_update_id():
    return update_id


def init():
    response = TelegramBot.getUpdates()
    if len(response) > 0:
        global update_id
        update_id = response[len(response) - 1]['update_id']
        update_id += 1
    TelegramBot.sendMessage(configs['telegram']['chat_id'], 'START RIG MONITOR  :D', reply_markup=markup)


def check_bot():
    try:
        response = TelegramBot.getUpdates(get_update_id())
        if len(response) > 0:
            for msg in response:
                handle(msg['message'])

            global update_id
            update_id = response[len(response) - 1]['update_id']
            update_id += 1
    except Exception as e:
        logger.error("Telegram ", e)


def send_message(text):
    try:
        TelegramBot.sendMessage(configs['telegram']['chat_id'], text, parse_mode='Markdown')
    except:
        logger.info("MSG: Send - ", text)
