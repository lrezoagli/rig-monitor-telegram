import schedule
import time
import yaml
import logging

import telegram_bot
from miners.claymore_miner import ClaymoreMiner
from model.info_rig import InfoRig

configs = None
TelegramBot = None

with open("config.yml", 'r') as stream:
    configs = yaml.load(stream)


def init_logger():
    logging.basicConfig(level=logging.INFO)
    handler = logging.FileHandler('claymore_miner.log')
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logging._addHandlerRef(handler)


def check_rig():
    text_alert = ""
    alert = False
    for r in configs['rigs']:
        info_rig = InfoRig(rig_id=r['rig_id'], rig_name=r['rig_name'], miner=r['miner'], ip=r['host'],
                           installed_gpus=r['installed_gpu'],
                           target_hashrate=r['target_hashrate'], target_hashrate_alt=r['target_hashrate_alt'],
                           target_temp=r['target_temp'])
        try:
            if r['miner'] == "claymore_cryptonight":

                miner = ClaymoreMiner(rig_name=r['rig_name'], host=r['host'], port=r['port'],
                                      installed_gpu=r['installed_gpu'])
                status_rig = miner.get_status_rig()
                status_gpu = miner.get_status_gpu()

                if info_rig.target_hashrate > int(status_rig.hashrate_mhs):
                    alert = True
                    text_alert += "*Hashrate is DOWN:*</b> " + str(
                        "{:,.2f}".format(status_rig.hashrate_mhs)) + ' *H/s*' + str(
                        " OF  {:,.2f}".format(info_rig.target_hashrate)) + ' *H/s*' + '\n'

                if info_rig.installed_gpus > status_rig.working_gpus:
                    alert = True
                    text_alert += "*Working GPU:* " + str(status_rig.working_gpus) + " OF " + str(
                        info_rig.installed_gpus) + '\n'

                for gg in status_gpu:
                    if int(gg.gpu_hashrate) == 0:
                        alert = True
                        text_alert += " *GPU:* " + str(gg.rig_gpu_id) + " is 0 H/s " + '\n'
                    if int(gg.gpu_temp) > int(info_rig.target_temp):
                        alert = True
                        text_alert += " *GPU TEMP:* " + str(gg.rig_gpu_id) + " C "

            else:
                miner = ClaymoreMiner(rig_name=r['rig_name'], host=r['host'], port=r['port'],
                                      installed_gpu=r['installed_gpu'])
                status_rig = miner.get_status_rig()
                status_gpu = miner.get_status_gpu()

                if info_rig.target_hashrate > int(status_rig.hashrate_mhs / float(1000)):
                    alert = True
                    text_alert += "*Hashrate is DOWN:*</b> " + str(
                        "{:,.2f}".format(status_rig.hashrate_mhs / float(1000))) + ' *Mh/s*' + str(
                        " OF  {:,.2f}".format(info_rig.target_hashrate)) + ' *Mh/s*' + '\n'

                if info_rig.installed_gpus > status_rig.working_gpus:
                    alert = True
                    text_alert += "*Working GPU:* " + str(status_rig.working_gpus) + " OF " + str(
                        info_rig.installed_gpus) + '\n'

                for gg in status_gpu:
                    if int(gg.gpu_hashrate) == 0:
                        alert = True
                        text_alert += " *GPU:* " + str(gg.rig_gpu_id) + " is 0 Mh/s " + '\n'
                    if int(gg.gpu_temp) > int(info_rig.target_temp):
                        alert = True
                        text_alert += " *GPU TEMP:* " + str(gg.rig_gpu_id) + " C "

        except:
            print("algun bajon paso...")
        if alert:
            telegram_bot.send_message(text_alert)
            # ssh.restart_server()


schedule.every(2).seconds.do(telegram_bot.check_bot)
schedule.every(60).minutes.do(telegram_bot.send_status)
schedule.every(1).minutes.do(check_rig)

if __name__ == '__main__':
    init_logger()
    logging.info("Start RIG MONITOR!!!!")
    telegram_bot.init()
    telegram_bot.send_full_status()
    while 1:
        schedule.run_pending()
        time.sleep(5)
