# Rig Monitor Telegram
Este pequeño soft sirve para poder monitorear tus rigs desde Telegram.
Tiene diferentes alertas, por Temperatura, por HashRate, o porque no responde el miner.
Cada una hora, te envia un status resumido del rig y si no, se le puede pedir que te envie una info detallada de como estan las GPU, que incluye, el pool, el uptime, la temp de cada placa, la vel de los fans, y el hashrate.

![EXAMPLE](example.png)


# Requerimientos:
  - Python3
     - Windows: https://www.howtogeek.com/197947/how-to-install-python-on-windows/
     - Linux: http://ubuntuhandbook.org/index.php/2017/07/install-python-3-6-1-in-ubuntu-16-04-lts/
 - Claymore:
    - Solo sirve para monitorear Claymore Ethash o Cryptonight

## Installation

Descargar el codigo https://gitlab.com/lrezoagli/rig-monitor-telegram/-/archive/master/rig-monitor-telegram-master.zip
Descomprimir.....

Instalar los paquetes de python. 
Desde una consola ( http://www.vozidea.com/abrir-consola-comandos-terminal-windows ), entrar a la carpeta que descomprimimos.. y ejecutar.
```sh
$ pip install -r requirements.txt
```

Una vez terminado ( puede tardar un rato ), vamos a crear el bot de telegram...

### Crear bot en telegram
Lo primero, y el paso imprescindible para crear nuestro bot para Telegram es recurrir a @BotFather. Para esto solo tienes que hacer clic en el enlace anterior, o escribir en tu navegador de internet la dirección https://telegram.me/botfather. Otra opción es buscar directamente en Telegram botfather.
Pulsa INICIAR en la parte inferior de la aplicación Telegram. Esto te lleva a las diferentes acciones que puedes emprender con BotFather. En general, cualquier bot para Telegram, que esté medianamente bien programado, debería mostrarte una ayuda como la que te muestra BotFather.
Llegados a este punto vamos a crear nuestro bot para Telegram. Para ello escribiremos un mensaje que sea /newbot. Esto nos devolverá un mensaje como el que ves en la siguiente captura de pantalla, donde únicamente nos está preguntando el nombre de nuestro bot.
Ahora nos pide que le pongamos un nombre de usuario, y debe cumplir con la condición de terminar en bot, por ejemplo MiBotDePruebaBot o prueba_bot. Probaré con este segundo nombre de usuario, sin embargo me da error. Como era de esperar, este nombre de bot ya está siendo utilizado por otro, así que tendremos que probar con otro, prueba20170909_bot
Perfecto, ya tenemos nuestro bot, y además nos han asignado un TOKEN que será el que utilicemos en nuestras aplicaciones. 
Crear un grupo de telegram y agregar al bot a ese nuevo grupo. 
Una vez el bot dentro del grupo escribir en un navegador


```json
https://api.telegram.org/bot<TOKEN>/getUpdates ( sacarle los <> )
```
Esto nos devuelve:
```json
"message": {
    "chat": {
        "id": -210987654,
        "title": ...,
        "type": "group",
        ...
    }
} 
```

Guardamos el ID, que esta dentro del chat. Ej: -210987654

### Configuracion del rig.

Abrimos el archivo config.yml con cualquier editor de texto.
```json
telegram:
  token: xxx
  chat_id: xxx
  ```
  TOKEN: ponemos el TOKEN del bot. 
  CHAT_ID: ponemos el ID del chat que sacamos antes. 
  
  ```json
  rigs:
  - rig_id: 1
    rig_name: rig1
    host: 192.168.0.10
    port: 3333
    installed_gpu: 8
    miner: claymore
    target_hashrate: 220
    target_hashrate_alt: 0
    target_temp: 65
  - rig_id: 2
    rig_name: rig2
    host: 192.168.0.12
    port: 3333
    installed_gpu: 8
    miner: claymore
    target_hashrate: 222
    target_hashrate_alt: 0
    target_temp: 65
  ```
rig_id: numero del rig. <br />
rig_name: nombre del rig<br />
host: ip del rig<br />
port: puerto del monitor del claymore<br />
target_hashrate:  aca va el hashrate que esperamos tener, recomiendo, poner 10 o 20 menos, que lo que dan las placas. Al bajar de este hashrate, el monitor, nos va a enviar una alerta por Telegram.<br />
target_hashrate_alt: No esta probado. <br />
target_temp: temperatura maxima, al superar esta temp, el monitor nos va a mandar una alerta por telegram<br />

# RUN FOREST RUN:
Ya casi estamos, para correr el monitor, escribir en una consola:
```sh
$ python3 status.py
```


# Donaciones:
ETH: 0x2a99309ae803ffdbf4973e1d0b3acb7a3110a94c
