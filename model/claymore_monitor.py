import datetime
import logging

from model.status_gpu import StatusGpu
from model.status_rig import StatusRig

logger = logging.getLogger(__name__)


class ClaymoreMonitor(object):
    def __init__(self, rig_name, rig_ip, miner_version, installed_gpu, mins_up, working_gpus, hashrate_mhs,
                 accepted_shares, rejected_shares, hashrate_mhs_alt, accepted_shares_alt, rejected_shares_alt,
                 gpu_hashrate_mhs, gpu_hashrate_mhs_alt, gpu_temperatures, gpu_fans, current_pools, num_invalid_shares,
                 num_pool_switches, num_invalid_shares_alt, num_pool_switches_alt):
        self._time = datetime.datetime.now()
        self._rig_name = rig_name
        self._rig_ip = rig_ip
        self._version = miner_version
        self._installed_gpu = installed_gpu
        self._mins_up = mins_up
        self._working_gpus = working_gpus

        self._hashrate_mhs = hashrate_mhs
        self._accepted_shares = accepted_shares
        self._rejected_shares = rejected_shares

        self._hashrate_mhs_alt = hashrate_mhs_alt
        self._accepted_shares_alt = accepted_shares_alt
        self._rejected_shares_alt = rejected_shares_alt

        self._gpu_hashrate_mhs = gpu_hashrate_mhs
        self._gpu_hashrate_mhs_alt = gpu_hashrate_mhs_alt

        self._gpu_temperatures = gpu_temperatures
        self._gpu_fans = gpu_fans

        self._current_pools = current_pools

        self._num_invalid_shares = num_invalid_shares
        self._num_pool_switches = num_pool_switches
        self._num_invalid_shares_alt = num_invalid_shares_alt
        self._num_pool_switches_alt = num_pool_switches_alt

    def get_status_rig(self):
        return StatusRig(time=self._time, rig_name=self._rig_name, mins_up=self._mins_up,
                         working_gpus=self._working_gpus,
                         hashrate_mhs=self._hashrate_mhs, accepted_shares=self._accepted_shares,
                         rejected_shares=self._rejected_shares, hashrate_mhs_alt=self._hashrate_mhs_alt,
                         accepted_shares_alt=self._accepted_shares_alt, rejected_shares_alt=self._rejected_shares_alt, pool=self._current_pools, miner_version=self._version)

    def get_status_gpu(self):
        gpus = []
        for i in range(0, self._working_gpus):

            if isinstance(self._gpu_hashrate_mhs_alt[i], int):
                gpu_hashrate_alt = self._gpu_hashrate_mhs_alt[i]
            else:
                gpu_hashrate_alt = 0

            status_gpu = StatusGpu(time=self._time, rig_gpu_id=self._rig_name + "/" + str(i),
                                   gpu_hashrate=self._gpu_hashrate_mhs[i],
                                   gpu_shares=self._accepted_shares,
                                   gpu_hashrate_alt=gpu_hashrate_alt,
                                   gpu_shares_alt=self._accepted_shares_alt,
                                   gpu_temp=self._gpu_temperatures[i],
                                   gpu_fan=self._gpu_fans[i])

            gpus.append(status_gpu)

        return gpus
