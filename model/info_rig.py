class InfoRig(object):
    def __init__(self, rig_id, rig_name, miner, ip, installed_gpus, target_hashrate, target_hashrate_alt, target_temp):
        self._rig_id = rig_id
        self._miner = miner
        self._ip = ip
        self._installed_gpus = installed_gpus
        self._target_hashrate = target_hashrate
        self._target_hashrate_alt = target_hashrate_alt
        self._target_temp = target_temp
        self._rig_name = rig_name

    @property
    def rig_name(self):
        return self._rig_name

    @property
    def rig_id(self):
        return self._rig_id

    @property
    def miner(self):
        return self._miner

    @property
    def ip(self):
        return self._ip

    @property
    def installed_gpus(self):
        return self._installed_gpus

    @property
    def target_hashrate(self):
        return self._target_hashrate

    @property
    def target_hashrate_alt(self):
        return self._target_hashrate_alt

    @property
    def target_temp(self):
        return self._target_temp
