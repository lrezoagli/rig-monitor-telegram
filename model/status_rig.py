import datetime


class StatusRig(object):
    def __init__(self, time, rig_name, mins_up, working_gpus, hashrate_mhs, accepted_shares, rejected_shares,
                 hashrate_mhs_alt, accepted_shares_alt, rejected_shares_alt, pool, miner_version):
        self._time = time
        self._rig_name = rig_name
        self._mins_up = mins_up
        self._working_gpus = working_gpus
        self._hashrate_mhs = hashrate_mhs
        self._accepted_shares = accepted_shares
        self._rejected_shares = rejected_shares
        self._hashrate_mhs_alt = hashrate_mhs_alt
        self._accepted_shares_alt = accepted_shares_alt
        self._rejected_shares_alt = rejected_shares_alt
        self._pool = pool
        self._miner_version = miner_version

    @property
    def pool(self):
        return self._pool

    @property
    def miner_version(self):
        return self._miner_version

    @property
    def mins_up(self):
        return self._mins_up

    @property
    def time(self):
        return self._time

    @time.setter
    def time(self, value):
        self._time = value

    @property
    def rig_name(self):
        return self._rig_name

    @rig_name.setter
    def rig_name(self, value):
        self._rig_name = value

    @property
    def working_gpus(self):
        return self._working_gpus

    @working_gpus.setter
    def working_gpus(self, value):
        self._working_gpus = value

    @property
    def hashrate_mhs(self):
        return self._hashrate_mhs

    @hashrate_mhs.setter
    def hashrate_mhs(self, value):
        self._hashrate_mhs = value

    @property
    def accepted_shares(self):
        return self._accepted_shares

    @accepted_shares.setter
    def accepted_shares(self, value):
        self._accepted_shares = value

    @property
    def rejected_shares(self):
        return self._rejected_shares

    @rejected_shares.setter
    def rejected_shares(self, value):
        self._rejected_shares = value

    @property
    def hashrate_mhs_alt(self):
        return self._hashrate_mhs_alt

    @hashrate_mhs_alt.setter
    def hashrate_mhs_alt(self, value):
        self._hashrate_mhs_alt = value

    @property
    def accepted_shares_alt(self):
        return self._accepted_shares_alt

    @accepted_shares_alt.setter
    def accepted_shares_alt(self, value):
        self._accepted_shares_alt = value

    @property
    def rejected_shares_alt(self):
        return self._rejected_shares_alt

    @rejected_shares_alt.setter
    def rejected_shares_alt(self, value):
        self._rejected_shares_alt = value
