class StatusGpu(object):
    def __init__(self, time, rig_gpu_id, gpu_hashrate, gpu_shares, gpu_hashrate_alt, gpu_shares_alt, gpu_temp, gpu_fan):
        self._time = time
        self._rig_gpu_id = rig_gpu_id
        self._gpu_hashrate = gpu_hashrate
        self._gpu_shares = gpu_shares
        self._gpu_hashrate_alt = gpu_hashrate_alt
        self._gpu_shares_alt = gpu_shares_alt
        self._gpu_temp = gpu_temp
        self._gpu_fan = gpu_fan

    @property
    def time(self):
        return self._time

    @property
    def rig_gpu_id(self):
        return self._rig_gpu_id

    @rig_gpu_id.setter
    def rig_gpu_id(self, value):
        self._rig_gpu_id = value

    @property
    def gpu_hashrate(self):
        return self._gpu_hashrate

    @gpu_hashrate.setter
    def gpu_hashrate(self, value):
        self._gpu_hashrate = value

    @property
    def gpu_shares(self):
        return self._gpu_shares

    @gpu_shares.setter
    def gpu_shares(self, value):
        self._gpu_shares = value

    @property
    def gpu_hashrate_alt(self):
        return self._gpu_hashrate_alt

    @gpu_hashrate_alt.setter
    def gpu_hashrate_alt(self, value):
        self._gpu_hashrate_alt = value

    @property
    def gpu_shares_alt(self):
        return self._gpu_shares_alt

    @gpu_shares_alt.setter
    def gpu_shares_alt(self, value):
        self._gpu_shares_alt = value

    @property
    def gpu_temp(self):
        return self._gpu_temp

    @gpu_temp.setter
    def gpu_temp(self, value):
        self._gpu_temp = value

    @property
    def gpu_fan(self):
        return self._gpu_fan

    @gpu_fan.setter
    def gpu_fan(self, value):
        self._gpu_fan = value
