#!/usr/local/bin/python3

import time
import json
import socket
import logging

import telegram_bot
from model.claymore_monitor import ClaymoreMonitor

logger = logging.getLogger(__name__)


class ClaymoreMiner(object):
    request = '{\"id\":0,\"jsonrpc\":\"2.0\",\"method\":\"miner_getstat1\"}'

    def __init__(self, rig_name, host, port, installed_gpu):
        # Config pasar a YML
        self.host = host
        self.port = port
        self.rig_name = rig_name
        self.installed_gpu = installed_gpu
        self._claymore_monitor = None
        self._initialize_logger()
        aux = self._get_stats()
        if aux:
            self._parse_stat()

    def _initialize_logger(self):

        logger.info('* * * * * * * * * * * * * * * * * * * *')
        logger.info('claymore logger instantiated')
        logger.info('monitoring session started')

    def _create_client(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect((self.host, self.port))

    def _parse_stat(self):
        try:

            version = self.stats['result'][0]
            mins_up = self.stats['result'][1]

            hashrate_mhs = int(self.stats['result'][2].split(';')[0])
            accepted_shares = int(self.stats['result'][2].split(';')[1])
            rejected_shares = int(self.stats['result'][2].split(';')[2])

            hashrate_mhs_alt = int(self.stats['result'][4].split(';')[0])
            accepted_shares_alt = int(self.stats['result'][4].split(';')[1])
            rejected_shares_alt = int(self.stats['result'][4].split(';')[2])

            gpu_hashrate_mhs = self.stats['result'][3].split(';')
            gpu_hashrate_mhs_alt = self.stats['result'][5].split(';')

            gpu_temperatures = self.stats['result'][6].split(';')[::2]
            gpu_fans = self.stats['result'][6].split(';')[1::2]

            current_pools = self.stats['result'][7]

            num_invalid_shares = int(self.stats['result'][8].split(';')[0])
            num_pool_switches = int(self.stats['result'][8].split(';')[1])
            num_invalid_shares_alt = int(self.stats['result'][8].split(';')[2])
            num_pool_switches_alt = int(self.stats['result'][8].split(';')[3])

            logger.debug("version: " + str(version))
            logger.debug("mins_up: " + str(mins_up))
            logger.debug("hashrate_mhs: " + str(hashrate_mhs))
            logger.debug("accepted_shares: " + str(accepted_shares))
            logger.debug("rejected_shares: " + str(rejected_shares))
            logger.debug("hashrate_mhs_alt: " + str(hashrate_mhs_alt))
            logger.debug("accepted_shares_alt: " + str(accepted_shares_alt))
            logger.debug("rejected_shares_alt: " + str(rejected_shares_alt))
            logger.debug("gpu_hashrate_mhs: " + str(gpu_hashrate_mhs))
            logger.debug("gpu_hashrate_mhs_alt: " + str(gpu_hashrate_mhs_alt))
            logger.debug("gpu_temperatures: " + str(gpu_temperatures))
            logger.debug("gpu_fans: " + str(gpu_fans))
            logger.debug("current_pools: " + str(current_pools))
            logger.debug("num_invalid_shares: " + str(num_invalid_shares))
            logger.debug("num_pool_switches: " + str(num_pool_switches))
            logger.debug("num_invalid_shares_alt: " + str(num_invalid_shares_alt))
            logger.debug("num_pool_switches_alt: " + str(num_pool_switches_alt))
            logger.debug("number_gpu: " + str(len(gpu_hashrate_mhs)))
            self._claymore_monitor = ClaymoreMonitor(rig_name=self.rig_name, rig_ip=self.host,
                                                     miner_version=str(version), installed_gpu=self.installed_gpu,
                                                     mins_up=mins_up, working_gpus=len(gpu_hashrate_mhs),
                                                     hashrate_mhs=hashrate_mhs, accepted_shares=accepted_shares,
                                                     rejected_shares=rejected_shares, hashrate_mhs_alt=hashrate_mhs_alt,
                                                     accepted_shares_alt=accepted_shares_alt,
                                                     rejected_shares_alt=rejected_shares_alt,
                                                     gpu_hashrate_mhs=gpu_hashrate_mhs,
                                                     gpu_hashrate_mhs_alt=gpu_hashrate_mhs_alt,
                                                     gpu_temperatures=gpu_temperatures, gpu_fans=gpu_fans,
                                                     current_pools=current_pools, num_invalid_shares=num_invalid_shares,
                                                     num_pool_switches=num_pool_switches,
                                                     num_invalid_shares_alt=num_invalid_shares_alt,
                                                     num_pool_switches_alt=num_pool_switches_alt)

        except Exception as e:
            return None

    def _get_stats(self):
        retries = 150

        while retries:
            try:
                self._create_client()
                self.client.sendall(self.request.encode())
                response = self.client.recv(512)
                stats = json.loads(response.decode('utf-8'))
                self.client.close()

                if not stats['error']:
                    self.stats = stats
                    return True
                else:
                    logger.error('could not get stats due to error {}'.format(stats['error']))
                    retries -= 1
                    return False

            except Exception as e:
                logger.error('could not connect to miner for the following reason:')
                logger.error(e)
                retries -= 1

        logger.error('connect retries exhausted, exiting...')
        telegram_bot.send_message("ALERT: Miner: " + self.rig_name + " NOT RESPOND")
        return False

    def get_status_rig(self):
        return self._claymore_monitor.get_status_rig()

    def get_status_gpu(self):
        return self._claymore_monitor.get_status_gpu()


if __name__ == '__main__':
    miner = ClaymoreMiner()
